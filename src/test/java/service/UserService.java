package service;

import dto.UserDTO;
import hooks.ServiceHooks;
import io.restassured.builder.RequestSpecBuilder;
import org.json.JSONObject;
import utils.PropertyReader;

import static io.restassured.RestAssured.given;

public class UserService {

    private final static String URL = PropertyReader.getProperty("urlApi") + "/api/users";

    public void getSingleUser() {
        RequestSpecBuilder request = new RequestSpecBuilder();

        request.setAccept("*/*");
        request.setContentType("application/json");

        ServiceHooks.setResponse(given().spec(request.build()).get(String.format("%s%s", URL, "/2")));

        System.out.println("Retorno da API Single User");
        ServiceHooks.getResponse().then().log().all();
    }

    public void getUsers() {
        RequestSpecBuilder request = new RequestSpecBuilder();

        request.setAccept("*/*");
        request.setContentType("application/json");

        ServiceHooks.setResponse(given().spec(request.build()).get(String.format("%s%s", URL, "?pages=2")));

        System.out.println("Retorno da API List Users");
        ServiceHooks.getResponse().then().log().all();
    }
    public void postCreateUser(UserDTO userDto) {
        RequestSpecBuilder request = new RequestSpecBuilder();

        request.setAccept("*/*");
        request.setContentType("application/json");

        ServiceHooks.setResponse(
                given()
                        .log()
                        .all()
                        .spec(request.build())
                        .body(userDto)
                        .post(URL));

        System.out.println("Retorno da API Create User");
        ServiceHooks.getResponse().then().log().all();
    }

    public void patchUser(UserDTO userDto, String newJob) {
        JSONObject requestParams = new JSONObject();
        requestParams.put("name", userDto.getName());
        requestParams.put("job", newJob);

        RequestSpecBuilder request = new RequestSpecBuilder();
        request.setAccept("*/*");
        request.setContentType("application/json");

        ServiceHooks.setResponse(
                given()
                        .log()
                        .all()
                        .spec(request.build())
                        .body(requestParams.toString())
                        .patch(String.format("%s%s", URL, "/2")));

        System.out.println("Retorno da API Patch User");
        ServiceHooks.getResponse().then().log().all();
    }

}
