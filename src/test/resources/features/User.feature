#language: pt
# Author: Carlos Moreira
# Date: 14/07/2023

@all
Funcionalidade: User

  @consultar
  Cenario: Consultar single user
    Dado que realizo a consulta na api single user
    Então valido o retorno da api com status code "200"
    E valido a estrutura do retorno da api single user

  @consultarListaUsuarios
  Cenario: Consultar lista de usuários
    Dado que realizo a consulta na api list users
    Então valido o retorno da api com status code "200"
    E valido a estrutura do retorno da api list users

  @cadastrar
  Cenario: Criar usuário
    Dado que realizo um cadastro de usuário
    Então valido o retorno da api com status code "201"
    E valido a criação do usuário
    E valido a estrutura do retorno da api create user

  @atualizar
  Cenario: Alterar cadastro de usuário parcialmente
    Dado que realizo um cadastro de usuário
    Quando altero o cadastro parcialmente
    Então valido o retorno da api com status code "200"
    E valido a alteração parcial do cadastro
    E valido a estrutura do retorno da api patch user
