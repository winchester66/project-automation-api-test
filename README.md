# Projeto de Teste Automatizados API
- API utilizada para teste: https://reqres.in/

## Finalidade
- [X] Validar o método POST
- [X] Validar o método GET SINGLE USER
- [X] Validar o método GET LIST USERS
- [X] Validar o método PATCH

## Ferramentas e Tecnologias Utilizadas

- [X] [Java]
- [X] [Gherkin]
- [X] [JavaFaker]
- [X] [Cucumber]
- [X] [RestAssured]
- [X] [IntelliJ] - Para codar

## Como executar os testes

- Em seu terminal execute o seguinte comando: mvn clean test

## Evidências

- Após rodar os testes é possível visualizar um HTML contendo as evidências dos últimos testes na pasta target/html

## Autor 

- Carlos Moreira 
- Linkedin: https://www.linkedin.com/in/carlos-moreira-887963122/
- Whatsapp: (14)99818-4411

Qualquer dúvida é só me chamar!
